export function getMin(arr) {
  return Math.min(...arr);
}

export const arrSum = arr => arr.reduce((a, b) => a + b, 0);
export function removeMin(arr, val, count) {
  arr.forEach((element, i) => {
    if (element === val && count > 0) {
      arr.splice(i, 1);
      count--;
    }
  });
  return arr;
}
