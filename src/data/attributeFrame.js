export const headers = [
  {
    abbr: "STR",
    label: "Strength"
  },
  {
    abbr: "DEX",
    label: "Dexterity"
  },
  {
    abbr: "CON",
    label: "Constitution"
  },
  {
    abbr: "INT",
    label: "Intelligence"
  },
  {
    abbr: "WIS",
    label: "Wisdom"
  },
  {
    abbr: "CHA",
    label: "Charisma"
  }
];
