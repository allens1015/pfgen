import { headers } from "../data/attributeFrame";
import rng from "../utils/rng";
import { getMin, removeMin, arrSum } from "../utils/arrayMutation";

// function applyRaceData(_attributeData, _characterData) {
//   if (_characterData.race.attributeScores.length == 7) {
//     console.log(_characterData);
//   } else {
//     console.log(_attributeData);
//   }
//   _attributeData.forEach(row => {
//     console.log(row);
//   });
//   return _attributeData;
// }

export default inputValues => {
  let attributeData = [];
  headers.forEach(header => {
    const row = {
      abbr: header.abbr,
      label: header.label,
      score: 10,
      displayModifier: "",
      intModifier: ""
    };
    attributeData.push(row);
  });
  if (inputValues.generationType == "dicePool") {
    attributeData.forEach(row => {
      let rollArr = [];
      for (let i = 0; i < inputValues.dicePool; i++) {
        rollArr.push(rng(1, 6));
      }
      if (inputValues.dropLowest) {
        const minVal = getMin(rollArr);
        rollArr = removeMin(rollArr, minVal, 1);
      }
      const scoreTotal = arrSum(rollArr);
      row.score = scoreTotal;
    });
    // attributeData = applyRaceData(attributeData, characterData);
  } else {
    let initialDistributionCount = 6;
    while (initialDistributionCount > 0) {
      const boostStat = rng(0, 5);
      attributeData[boostStat].score++;
      initialDistributionCount--;
    }
  }
  return attributeData;
};
