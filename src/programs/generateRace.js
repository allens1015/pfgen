import rng from "../utils/rng";
import raceObj from "../data/races";

export default _inputValues => {
  console.log(_inputValues);
  const raceId = rng(0, raceObj.races.length);
  return raceObj.races[raceId];
};
