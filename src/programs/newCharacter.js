import generateAttributes from "./generateAttributes";
import generateRace from "./generateRace";

export default inputValues => {
  const characterData = {};
  characterData.race = generateRace(inputValues);
  characterData.attributeScores = generateAttributes(inputValues);
  return characterData;
};
