import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    characterData: {
      attributeScores: [],
      race: false
    },
    inputValues: {
      pointBuy: 20,
      level: 1,
      dicePool: 4,
      generationType: "dicePool",
      dropLowest: false,
      rid: false
    }
  },
  getters: {
    getCharacterData: state => state.characterData,
    getInputValues: state => state.inputValues
  },
  mutations: {
    setCharacterData(state, payload) {
      state.characterData = payload;
    },
    setInputs(state, payload) {
      state.inputValues = payload;
    },
    setAttributes(state, payload) {
      state.characterData.attributeScores = payload;
    },
    setRace(state, payload) {
      state.characterData.race = payload;
    }
  }
});
